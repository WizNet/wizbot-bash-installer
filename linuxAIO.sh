#!/bin/sh
echo ""
echo "Welcome to WizBot."
echo "Downloading the latest installer..."
root=$(pwd)

rm "$root/w-menu.sh" 1>/dev/null 2>&1
wget -N https://gitlab.com/WizNet/wizbot-bash-installer/-/raw/v5/w-menu.sh

bash w-menu.sh
cd "$root"
rm "$root/w-menu.sh"
exit 0
