#!/bin/sh

root=$(pwd)

wget -q -N https://gitlab.com/WizNet/wizbot-bash-installer/-/raw/v5/detectOS.sh
bash detectOS.sh > /dev/null || exit $?


# remove old backup but store the database
if [ -d wizbot_old/output/data/ ]; then
    if [ -f wizbot_old/output/data/WizBot.db ]; then
        if [ ! -d wizbot_db_backups/ ]; then
            mkdir wizbot_db_backups
        fi
        date_now=$(date +%s)
        cp wizbot_old/output/data/WizBot.db wizbot_db_backups/WizBot-"$date_now".db

        if [ -f wizbot_old/output/creds.yml ]; then
            cp wizbot_old/output/creds.yml wizbot_db_backups/creds-"$date_now".yml
        fi
    fi
fi
rm -rf wizbot_old 1>/dev/null 2>&1

# make a new backup
mv -fT wizbot wizbot_old 1>/dev/null 2>&1

# clone new version
git clone -b v5 --recursive --depth 1 https://gitlab.com/WizNet/WizBot

wget -q -N https://gitlab.com/WizNet/wizbot-bash-installer/-/raw/v5/rebuild.sh
bash rebuild.sh

cd "$root"
rm "$root/w-download.sh"
exit 0
