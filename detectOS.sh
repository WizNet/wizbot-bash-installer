#!/bin/bash -e

function detect_OS_ARCH_VER_BITS {
    ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')

    if [ -f /etc/lsb-release ]; then
        . /etc/lsb-release
    fi

    if ! [ "$DISTRIB_ID" = "" ]; then
        OS=$DISTRIB_ID
        VER=$DISTRIB_RELEASE
    elif [ -f /etc/debian_version ]; then
        OS=Debian  # XXX or Ubuntu??
        VER=$(cat /etc/debian_version)
        SVER=$( grep -oP "[0-9]+" /etc/debian_version | head -1 )
    elif [ -f /etc/fedora-release ]; then
        OS=Fedora
        VER=$( grep -oP "[0-9]+" /etc/fedora-release | head -1 )
    elif [ -f /etc/os-release ]; then
        . /etc/os-release
        if [ "$NAME" = "" ]; then
            OS=$(uname -s)
            VER=$(uname -r)
        else
            OS=$NAME
            VER=$VERSION_ID
        fi
    else
        OS=$(uname -s)
        VER=$(uname -r)
    fi
    case $(uname -m) in
    x86_64)
        BITS=64
        ;;
    i*86)
        BITS=32
        ;;
    armv*)
        BITS=32
        ;;
    *)
        BITS=?
        ;;
    esac
    case $(uname -m) in
    x86_64)
        ARCH=x64  # or AMD64 or Intel64 or whatever
        ;;
    i*86)
        ARCH=x86  # or IA32 or Intel32 or whatever
        ;;
    *)
        # leave ARCH as-is
        ;;
    esac
}

function RM_SCRIPTS {
    rm -f w-prereq.sh
    rm -f detectOS.sh
}

declare OS ARCH VER BITS

detect_OS_ARCH_VER_BITS

export OS ARCH VER BITS

if [ "$BITS" = 32 ]; then
    echo -e "Your system architecture is $ARCH which is unsupported to run Microsoft .NET Core SDK. \nYour OS: $OS \nOS Version: $VER"
    echo
    printf "\e[1;31mPlease check the WizBot self-hosting guide for alternatives.\e[0m\n"
    rm w-prereq.sh
    exit 1
fi

declare OS ARCH VER BITS

detect_OS_ARCH_VER_BITS

export OS ARCH VER BITS

if [ "$OS" = "Ubuntu" ]; then
    supported_ver=("20.04" "22.04" "24.04")

    if [[ "${supported_ver[*]}" =~ ${VER} ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$OS" = "LinuxMint" ]; then
    SVER=$( echo "$VER" | grep -oP "[0-9]+" | head -1 )
    supported_ver=("19" "20" "21")

    if [[ "${supported_ver[*]}" =~ ${SVER} ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$OS" = "openSUSE Leap" ]; then
    supported_ver=("15.5" "15.6")

    if [[ "${supported_ver[*]}" =~ ${VER} ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$OS" = "AlmaLinux" ] || [ "$OS" = "Rocky Linux" ]; then
    SVER=$( echo "$VER" | grep -oP "[0-9]+" | head -1 )
    supported_ver=("8" "9")

    if [[ "${supported_ver[*]}" =~ ${SVER} ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$OS" = "Fedora" ]; then
    supported_ver=("38" "39" "40" "41" "42")

    if [[ "${supported_ver[*]}" =~ ${VER} ]]; then
        supported=1
    else
        supported=0
    fi
fi

if [ "$supported" = 0 ]; then
    {
        echo "Your OS $OS $VER $ARCH looks unsupported to run Microsoft .NET Core."
        echo "Exiting..."
        echo -e "\e[1;31mContact WizBot's support on Discord with screenshot.\e[0m\n"
    } >&2
    RM_SCRIPTS
    exit 1
fi

if [ "$OS" = "Linux" ]; then
    {
        echo "Your OS $OS $VER $ARCH probably can run Microsoft .NET Core."
        echo "Exiting..."
        echo "Contact WizBot's support on Discord with screenshot."
    } >&2
    RM_SCRIPTS
    exit 1
fi

echo "$OS"
echo "$VER"
echo "$ARCH"
echo "$SVER"

RM_SCRIPTS
exit 0
